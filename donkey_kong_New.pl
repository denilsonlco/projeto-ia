salvarPrincesa(Mario,ListaObjetivo,Solucao) :- verificaObjetivos(Mario,ListaObjetivo,Solucao,[]).

verificaObjetivos(_,Objetivos,CaminhoCompleto,Caminho) :-  Objetivos == [],CaminhoCompleto = Caminho.

verificaObjetivos(EstadoInicial,Objetivos,CaminhoCompleto,CaminhoParcial) :- buscaProfundidade([],EstadoInicial,Objetivos,CaminhoObjetivo),excluiObjetivo(CaminhoObjetivo,Objetivos,NovosObjetivos,NovoEstado),concatenaCaminhos(CaminhoObjetivo,CaminhoParcial,CaminhoResultante),verificaObjetivos(NovoEstado,NovosObjetivos,CaminhoCompleto,CaminhoResultante).

buscaProfundidade(Caminho,Estado,Objetivos,[Estado|Caminho]) :- meta(Estado, Objetivos).
buscaProfundidade(Caminho,Estado,Objetivos,Solucao) :- movimenta(Estado,Sucessor,[[3,3],[5,3],[6,5],[7,2],[8,4]],[[3,5],[7,1]],[[1,2],[1,3],[4,1],[4,2],[4,5],[7,3],[7,4],[9,1],[9,2],[10,2],[10,3]]),not(pertence(Sucessor,Caminho)),buscaProfundidade([Estado|Caminho],Sucessor,Objetivos,Solucao).

meta(Elem, [Elem|_]).

%Andar_para_direita
movimenta([Px,Py],[Px2,Py2],Barril,Parede,_) :- not(pertence([Px,Py],Barril)),not(pertence([Px,Py],Parede)),Px<10,Px2 is Px+1,Py2 is Py.

%Andar_para_esquerda
movimenta([Px,Py],[Px2,Py2],Barril,Parede,_) :- not(pertence([Px,Py],Barril)),not(pertence([Px,Py],Parede)),Px>1,Px2 is Px-1, Py2 is Py.

%Pular_barril
movimenta([Px,Py],[Px2,Py2],Barril,Parede,_) :- pertence([Px,Py], Barril),not(pertence([Px2,Py2], Barril)),Px<10,Px2 is Px+1,Py2 is Py.

%Pular_barril
movimenta([Px,Py],[Px2,Py2],Barril,Parede,_) :- pertence([Px,Py], Barril),not(pertence([Px2,Py2], Barril)),Px>1,Px2 is Px-1,Py2 is Py.

%Subir_Escada
movimenta([Px,Py],[Px2,Py2],_,_,Escada) :- pertence([Px,Py],Escada),Py<5, Py2 is Py+1,Px2 is Px.

%Descer_Escada
movimenta([Px,Py],[Px2,Py2],_,_,Escada) :- pertence([Px,Py],Escada),Py>1, Py2 is Py-1,Px2 is Px.

%Pertence_a_lista
pertence(Elem,[Elem|_]).
pertence(Elem,[_|Cauda]) :- pertence(Elem,Cauda).

%Exclui_elemento_da_lista
excluiObjetivo([Elem|_],[Elem|Cauda],Cauda,Elem).

%Concatena_duas_listas
concatenaCaminhos([],L,L).
concatenaCaminhos([Cab|Cauda],L2,[Cab|Resultado]):- concatenaCaminhos(Cauda,L2,Resultado).
